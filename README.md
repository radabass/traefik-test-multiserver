# This is a template for multiple apache-nginx servers on the same host thanks to Traefik

## Edit your hosts file

Start adding.

`127.0.0.1 env.local`

`127.0.0.1 server1.env.local`

`127.0.0.1 server2.env.local`

---

## Create the network if required

Next, you’ll add the new network if required.

`docker network create proxy`

---

## GO!

`docker-compose up`

And you will have Apache and Nginx running on the port 80 flawlessly.

